<a href="https://gitlab.com/jfeston/renegade">
  <img
    src="http://d3kq2xhl2rew87.cloudfront.net/team/logo/9/0/6/250x250/twitch_logo_ULYAioN.png"
    alt="Renegade" width=100
    title="Renegade" align="right" />
</a>

**Got a question?** Open an issue on [GitLab](https://gitlab.com/jfeston/renegade/issues)

# Introduction

~~renegade~~ is an implementation of IdentityServer4, a .NET library that allows 
implementing single sign-on and access control for modern web applications and 
APIs using standard specifications like OpenID Connect (1.0) and OAuth (2.0)

# Questions and issues

I am currently tracking issues in [GitLab](https://gitlab.com/jfeston/renegade/issues). 
Extended documentation will be added as the project progresses.

## Kanban dashboard

I mantain a dashboard in Trello to keep track of the work I'm doing on the 
project: [https://trello.com/b/odgEnMkA](https://trello.com/b/odgEnMkA) 

# Contribute

Contributions are always welcome! Please read the contribution guidelines first.

