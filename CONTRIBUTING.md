# Contributing to this project

## About

This document provides a set of best practices for open source contributions - bug reports, code submissions / pull requests, etc.

Naturally, this document is itself open source, and we encourage feedback & suggestions for improvement.

### Sources

Currently this document is an adaptation from the contribution documentation of [https://contribution-guide-org.readthedocs.io/](https://contribution-guide-org.readthedocs.io/).

It's expected that over time I will incorporate additional material from related attempts at consolidating this type of info. I'll update with a list here when that happens.

## Submitting bugs

### Due diligence

Before submitting a bug, please do the following:

- Perform **basic troubleshooting** steps:
    - **Make sure you’re on the latest version.** If you're not on the most recent version, your problem may have been solved already! Upgrading (if there is an option) is always the best first step. 
    - **Try older versions.** If you’re already on the latest release, try rolling back a few minor versions (e.g. if on 1.7, try 1.5 or 1.6) and see if the problem goes away. This will help the devs narrow down when the problem first arose in the commit log.
    - **Try switching up dependency versions.** If the software in question has dependencies (other libraries, etc) try upgrading/downgrading those as well.
- **Search the project's bug/issue tracker** to make sure it's not a known issue.

