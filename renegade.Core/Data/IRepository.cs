﻿using System;
using System.Collections.Generic;
using System.Text;

namespace renegade.Core.Data
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Update(T entity);
        T Update(int id, Func<T, T> onFoundUpdate);
        long Insert(T entity);
        void Delete(T entity);

        string ConnectionString { get; set; }
    }
}
